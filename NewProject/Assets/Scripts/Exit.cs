using UnityEngine;

public class Exit : MonoBehaviour {
    
    void Update () {
        if (Input.GetKey(KeyCode.Escape)) {
            Cursor.lockState = CursorLockMode.None;
            SceneLoader.LoadScene(Scenes.Menu);
        }
    }
}
