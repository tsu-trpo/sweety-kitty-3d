using UnityEngine;

public class Coin: BoostBehaviour {
    
    public override void Execute () {
        EventManager.TriggerEvent(EventName.pick_bonus);
    }
}