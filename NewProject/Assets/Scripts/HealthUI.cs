﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HealthUI : MonoBehaviour, IObserver {
    
    private Text text;
    
    private void Start () {
        text = GetComponent<Text>();
        ServiceLocator.GetService<HealthController>().AddObserver(this);
    }
    
    public void Action (int health) {
        text.text = health.ToString();
    }
    
    private void OnDestroy () {
        ServiceLocator.GetService<HealthController>().RemoveObserver(this);
    }
}

