﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Restarter : MonoBehaviour, IObserver {
    
    void Start () {
        ServiceLocator.GetService<HealthController>().AddObserver(this);
    }
    
    public void Action (int health) {
        if (health <= 0) {
            Cursor.lockState = CursorLockMode.None;
            SceneLoader.LoadScene(Scenes.RetryMenu);
        }
    }
    
    private void OnDestroy () {
        ServiceLocator.GetService<HealthController>().RemoveObserver(this);
    }
}
