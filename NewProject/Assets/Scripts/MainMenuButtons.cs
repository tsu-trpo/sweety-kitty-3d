﻿using UnityEngine;

public class MainMenuButtons : MonoBehaviour {
    
    public void NewGame () {
        SceneLoader.LoadScene(Scenes.Game);
    }
    
    public void Quit () {
        Application.Quit();
    }
}
