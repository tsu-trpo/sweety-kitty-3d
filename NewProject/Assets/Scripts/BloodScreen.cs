﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodScreen : MonoBehaviour, IObserver {
    
    private Image image;
    
    void Start () {
        image = GetComponent<Image>();
        ServiceLocator.GetService<HealthController>().AddObserver(this);
    }
    
    void Update () {
        if (image.color != Color.clear) {
            image.color = Color.Lerp(image.color, Color.clear, Time.deltaTime);
        }
    }
    
    public void Action (int health) {
        image.color = Color.red;
    }
    
    private void OnDestroy () {
        ServiceLocator.GetService<HealthController>().RemoveObserver(this);
    }
}
