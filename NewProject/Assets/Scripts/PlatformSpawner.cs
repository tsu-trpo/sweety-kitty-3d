﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class PlatformSpawner : MonoBehaviour {
    public GameObject platformPrefab;
    public GameObject[] enemyPrefabs;
    public GameObject[] bonusItemPrefabs;
    public Transform[] spawnPoints;
    List <Vector3> centers;

    void Start () {
        centers = new List<Vector3>();       
        centers.AddRange(spawnPoints.Select(x => x.position));
        InvokeRepeating("Spawning", 0.5f, 0.5f);	
    }

    
    private void Spawning(){
        for (int i=0; i< spawnPoints.Length; i++){
            spawnPoints[i].position = SetNewPosition (centers[i]);			
        }
		
        for (int i=0; i < spawnPoints.Length; i++){
            Instantiate(platformPrefab, spawnPoints[i].position, spawnPoints[i].rotation);
            if (Random.Range(0,8)==0) ChoosePrefab(i);
        }
    }

    private Vector3 SetNewPosition (Vector3 center){
        float distance = 8.5f;
        Vector3 newPosition = center;
        float xPosition = Random.Range ( -distance, distance);
        float zPosition = Mathf.Sqrt (Mathf.Pow (distance, 2) - Mathf.Pow (xPosition, 2));
        zPosition = Random.Range (-zPosition, zPosition);
        newPosition.x += xPosition;
        newPosition.z += zPosition;
        return newPosition;		
    }

    private void ChoosePrefab (int i){
        if (Random.Range(0,2)>0){
            int bonus = Random.Range(0,bonusItemPrefabs.Length);
            Instantiate(bonusItemPrefabs[bonus], spawnPoints[i].position, spawnPoints[i].rotation);	
        }
        else {
            int enemy = Random.Range(0,enemyPrefabs.Length);
            Instantiate(enemyPrefabs[enemy], spawnPoints[i].position, spawnPoints[i].rotation);
        }
    }
}			