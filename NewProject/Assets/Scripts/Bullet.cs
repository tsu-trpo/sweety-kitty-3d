﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {
    
    private Rigidbody rigidBody;
    private Vector3 initialPosition;
    private float speed = 20f;
    private float shootDistance = 100;
    
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        Vector3 velocity = Vector3.forward * speed;
        velocity = Quaternion.Euler(transform.rotation.eulerAngles) * velocity;
        rigidBody.velocity = velocity;
    }
    
    void Update () {
        if (Vector3.Distance(transform.position, initialPosition) > shootDistance){
            Destroy(this.gameObject);
        }
    }
    
    private void OnCollisionEnter (Collision collision) {
        Destroy(this.gameObject);
    }
}
