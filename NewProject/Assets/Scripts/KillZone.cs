using UnityEngine;

public class KillZone : MonoBehaviour {
    
    private HealthController healthController;
    
    void Start(){
        healthController = ServiceLocator.GetService<HealthController>();
    }
	
    private void OnCollisionEnter(Collision collision){
        if ( collision.transform.tag == "Player") {
            healthController.Damage(1000f);
        }
        else{
            Destroy(collision.gameObject);
        }
    }	
}