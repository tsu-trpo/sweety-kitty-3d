﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles:MonoBehaviour {
    
    private float enterDamage = 1000f;
    private float stayDamage = 5f;
    public HealthController healthController;
    void OnCollisionStay (Collision collision) {
        if(collision.transform.tag == "Player") {
            healthController.Damage(stayDamage*Time.fixedDeltaTime);
        }
    }
    
    void OnCollisionEnter (Collision collision) {
        if(collision.transform.tag == "Player") {
            healthController.Damage(enterDamage*Time.fixedDeltaTime);
        }
    }

    void Start () {
        healthController = ServiceLocator.GetService<HealthController>();
    }
}