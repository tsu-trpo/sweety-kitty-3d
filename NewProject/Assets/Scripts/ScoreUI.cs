using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreUI : MonoBehaviour, IObserver {
    
    private Text text;
    
    private void Start () {
        text = GetComponent<Text>();
        ServiceLocator.GetService<Score>().AddObserver(this);
    }
    
    public void Action (int score) {
        text.text = "Score: " + score.ToString();
    }
    
    private void OnDestroy () {
        ServiceLocator.GetService<Score>().RemoveObserver(this);
    }
}
