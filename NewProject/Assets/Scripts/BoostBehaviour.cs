﻿using UnityEngine;

public abstract class BoostBehaviour : MonoBehaviour {
    
    void OnCollisionEnter (Collision collision) {
        if(collision.transform.tag == "Player") {
            Execute();
            Destroy(this.gameObject);
        }
    }
    
    abstract public void Execute();
}