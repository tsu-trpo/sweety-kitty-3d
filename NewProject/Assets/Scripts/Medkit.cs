using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit: BoostBehaviour {
    
    private float regen = 5f;
    public HealthController healthController;
    
    void Start () {
        healthController = ServiceLocator.GetService<HealthController>();
    }
    
    public override void Execute () {
        healthController.AddHealth(regen);
        EventManager.TriggerEvent(EventName.pick_bonus);
    }
}
