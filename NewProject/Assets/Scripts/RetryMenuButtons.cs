﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetryMenuButtons : MonoBehaviour {
    
    public void Restart () {
        SceneLoader.LoadScene(Scenes.Game);
    }
    
    public void BackToMain () {
        SceneLoader.LoadScene(Scenes.Menu);
    }
}
