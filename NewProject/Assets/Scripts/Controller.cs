﻿using UnityEngine;

public class Controller : MonoBehaviour {
    
    private Rigidbody rigidBody;
    private MouseLook look;
    private bool isJumping = false;
    
    private float moveSpeed = 5f;
    private float initialJumpSpeed = 5f;
    
    void Start () {
        look = GetComponent<MouseLook>();
        rigidBody = GetComponent<Rigidbody>();
    }
    
    void Update () {
        if (!isJumping) {
            Moving();
        }
    }
    
    private void OnCollisionStay (Collision other) {
        if (Input.GetKey(KeyCode.Space) && !isJumping) {
            rigidBody.AddForce(other.contacts[0].normal * initialJumpSpeed, ForceMode.VelocityChange);
            isJumping = true;
        }
    }
    
    private void OnCollisionEnter (Collision other) {
        if (isJumping) {
            isJumping = false;
        }
    }
    
    private void Moving () {
        Vector3 velocity = Vector3.zero;
        velocity.x = Input.GetAxis("Horizontal") * moveSpeed;
        velocity.z = Input.GetAxis("Vertical") * moveSpeed;
        velocity = Quaternion.Euler(0, look.currRotation.y, 0) * velocity;
        velocity.y = rigidBody.velocity.y;
        rigidBody.velocity = velocity;
    }
}