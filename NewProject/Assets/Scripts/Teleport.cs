using UnityEngine;

public class Teleport : MonoBehaviour {

    public Transform destination;
    public GameObject PlatformPrefab;	
	
    private void OnTriggerEnter(Collider collider){
        if( collider.transform.tag == "Player"){
            collider.gameObject.transform.position = destination.position;
            Instantiate(PlatformPrefab, destination.position+Vector3.down*2f,destination.rotation);
        }
    }	
}