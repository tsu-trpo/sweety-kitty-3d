using UnityEngine;

public class MouseLook : MonoBehaviour {
    
    public Vector2 currRotation;
    private float lookSensitivity = 2f;
    private Camera camera;
    
    void Start () {
        camera = GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update () {
        currRotation.x -= Input.GetAxis("Mouse Y") * lookSensitivity;
        currRotation.y += Input.GetAxis("Mouse X") * lookSensitivity;
        currRotation.x = Mathf.Clamp(currRotation.x, -60, 60);
        
        camera.transform.rotation = Quaternion.Euler(currRotation);
    }
}
