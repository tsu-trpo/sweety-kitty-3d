﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Image))]
public class HeartAnimator : MonoBehaviour, IObserver {
    
    private Animator animator;
    
    void Start () {
        animator = GetComponent<Animator>();
        ServiceLocator.GetService<HealthController>().AddObserver(this);
    }
    
    public void Action (int health) {
        float speed = 110 / (Mathf.Abs(health) + 10);
        animator.speed = speed;
    }
    
    private void OnDestroy () {
        ServiceLocator.GetService<HealthController>().RemoveObserver(this);
    }
}
