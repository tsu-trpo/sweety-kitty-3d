﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour, IObservable {
    
    private float health = 100f;
    private float maxHealth = 100f;
    private float bulletDamage = 10f;
    
    private List<IObserver> observers = new List<IObserver>();
    
    public void AddHealth (float value) {
        health += value;
        if (health > maxHealth) {
            health = maxHealth;
        }
        Notify();
    }
    
    public void Damage (float damage) {
        health -= damage;
        Notify();
    }
    
    private void OnCollisionEnter (Collision collision) {
        if (collision.transform.tag == "bullet") {
            Damage(bulletDamage);
        }
    }
    
    public void AddObserver (IObserver o) {
        observers.Add(o);
    }
    
    public void RemoveObserver (IObserver o) {
        observers.Remove(o);
    }
    
    public void  Notify () {
        foreach (IObserver o in observers) {
            o.Action(Mathf.FloorToInt(health));
        }
    }
}
