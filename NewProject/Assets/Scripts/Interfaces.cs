using UnityEngine;

public interface IObserver {
    void Action(int value);
}

public interface IObservable {
    void AddObserver(IObserver o);
    void RemoveObserver(IObserver o);
    void Notify();
}