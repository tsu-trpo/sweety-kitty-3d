using UnityEngine;

public class Platform : MonoBehaviour {
    
    private float speed = 2f;
    private Rigidbody rigidBody;

    void Start () {
        rigidBody = GetComponent <Rigidbody>();
        rigidBody.velocity = Vector3.down * speed;		
    }
}