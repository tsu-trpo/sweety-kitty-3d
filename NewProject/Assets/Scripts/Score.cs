﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Score : MonoBehaviour, IObservable {
    
    private UnityAction enemyListener;
    private UnityAction bonusListener;
    
    private List<IObserver> observers = new List<IObserver>();
    
    private int scoreForBonusPick = 20;
    private int scoreForEnemyKill = 100;
    private int score = 0;
    
    private void Start () {
        enemyListener = new UnityAction(EnemyKilled);
        bonusListener = new UnityAction(BonusPickedUp);
        EventManager.StartListening(EventName.pick_bonus, bonusListener);
        EventManager.StartListening(EventName.kill_enemy, enemyListener);
    }
    
    private void EnemyKilled () {
        score += scoreForEnemyKill;
        Notify();
    }
    
    private void BonusPickedUp () {
        score += scoreForBonusPick;
        Notify();
    }
    
    public void AddObserver (IObserver o) {
        observers.Add(o);
    }
    
    public void RemoveObserver (IObserver o) {
        observers.Remove(o);
    }
    
    public void Notify () {
        foreach(IObserver o in observers) {
            o.Action(score);
        }
    }
    
    private void OnDestroy() {
        EventManager.StopListening(EventName.pick_bonus, bonusListener);
        EventManager.StopListening(EventName.kill_enemy, enemyListener);
    }
}