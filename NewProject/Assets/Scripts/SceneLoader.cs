﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenes {
    public const int Menu = 0;
    public const int Game = 1;
    public const int RetryMenu = 2;
}

public static class SceneLoader {
    
    public static void LoadScene (int scene) {
        SceneManager.LoadScene(scene);
    }
}
