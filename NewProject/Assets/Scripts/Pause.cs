using UnityEngine;

public class Pause : MonoBehaviour {
    
    private float pausedTime;
    
    void Update () {
        if (Input.GetKeyDown(KeyCode.P)) {
            if (Time.timeScale > 0) {
                pausedTime = Time.timeScale;
                Time.timeScale = 0;
            }
            else {
                Time.timeScale = pausedTime;
            }
        }
    }
}