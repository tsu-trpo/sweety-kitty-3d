﻿using UnityEngine;

public class ShootController : MonoBehaviour {
    
    private enum State {
        Idle,
        Shooting
    }
    
    public GameObject bullet;
    public Transform shootPoint;
    private MouseLook look;
    private State state;
    private float shootTimeout = 0.7f;
    private float shootDeltaTime = 0f;
    
    void Start () {
        look = GetComponent<MouseLook>();
        state = State.Idle;
    }
    
    void Update () {
        if (state == State.Idle) {
            UpdateIdle();
        } else if (state == State.Shooting) {
            UpdateShooting();
        }
    }
    
    private void UpdateIdle () {
        if (Input.GetKey(KeyCode.Mouse0)) {
            Shoot();
            state = State.Shooting;
        }
    }
    
    private void UpdateShooting () {
        shootDeltaTime += Time.deltaTime;
        if (shootDeltaTime >= shootTimeout) {
            shootDeltaTime = 0f;
            state = State.Idle;
        }
    }
    
    private void Shoot () {
        Instantiate(bullet, shootPoint.position, Quaternion.Euler(look.currRotation));
    }
}
